/* J:\TestProjects\treecc\examples\expr_cpp.h.  Generated automatically by treecc */
#ifndef __yy_J__TestProjects_treecc_examples_expr_cpp_h
#define __yy_J__TestProjects_treecc_examples_expr_cpp_h


/*
 * Value that is computed by "eval_expr" below.
 */
typedef union
{
	int		int_value;
	float	float_value;

} eval_value;


#include <new>

const int expression_kind = 5;
const int binary_kind = 6;
const int unary_kind = 7;
const int intnum_kind = 8;
const int floatnum_kind = 9;
const int cast_kind = 16;
const int plus_kind = 10;
const int minus_kind = 11;
const int multiply_kind = 12;
const int divide_kind = 13;
const int power_kind = 14;
const int negate_kind = 15;

class expression;
class binary;
class unary;
class intnum;
class floatnum;
class cast;
class plus;
class minus;
class multiply;
class divide;
class power;
class negate;
typedef enum {
	error_type,
	int_type,
	float_type,
} type_code;


class YYNODESTATE
{
public:

	YYNODESTATE();
	virtual ~YYNODESTATE();

private:

	struct YYNODESTATE_block *blocks__;
	struct YYNODESTATE_push *push_stack__;
	int used__;
public:

	intnum *intnumCreate(int num);
	floatnum *floatnumCreate(float num);
	cast *castCreate(type_code new_type, expression * expr);
	plus *plusCreate(expression * expr1, expression * expr2);
	minus *minusCreate(expression * expr1, expression * expr2);
	multiply *multiplyCreate(expression * expr1, expression * expr2);
	divide *divideCreate(expression * expr1, expression * expr2);
	power *powerCreate(expression * expr1, expression * expr2);
	negate *negateCreate(expression * expr);

public:

	void *alloc(size_t);
	void dealloc(void *, size_t);
	int push();
	void pop();
	void clear();
	virtual void failed();
	virtual char *currFilename();
	virtual long currLinenum();

};

class expression
{
protected:

	int kind__;
	char *filename__;
	long linenum__;

public:

	int getKind() const { return kind__; }
	const char *getFilename() const { return filename__; }
	long getLinenum() const { return linenum__; }
	void setFilename(char *filename) { filename__ = filename; }
	void setLinenum(long linenum) { linenum__ = linenum; }

protected:

	friend class YYNODESTATE;

	expression(YYNODESTATE *state__);

public:

	type_code type;

	virtual eval_value eval_expr() = 0;

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~expression();

};

class binary : public expression
{
protected:

	friend class YYNODESTATE;

	binary(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	expression * expr1;
	expression * expr2;

	virtual eval_value eval_expr() = 0;

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~binary();

};

class unary : public expression
{
protected:

	friend class YYNODESTATE;

	unary(YYNODESTATE *state__, expression * expr);

public:

	expression * expr;

	virtual eval_value eval_expr() = 0;

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~unary();

};

class intnum : public expression
{
protected:

	friend class YYNODESTATE;

	intnum(YYNODESTATE *state__, int num);

public:

	int num;

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~intnum();

};

class floatnum : public expression
{
protected:

	friend class YYNODESTATE;

	floatnum(YYNODESTATE *state__, float num);

public:

	float num;

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~floatnum();

};

class cast : public expression
{
protected:

	friend class YYNODESTATE;

	cast(YYNODESTATE *state__, type_code new_type, expression * expr);

public:

	type_code new_type;
	expression * expr;

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~cast();

};

class plus : public binary
{
protected:

	friend class YYNODESTATE;

	plus(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~plus();

};

class minus : public binary
{
protected:

	friend class YYNODESTATE;

	minus(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~minus();

};

class multiply : public binary
{
protected:

	friend class YYNODESTATE;

	multiply(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~multiply();

};

class divide : public binary
{
protected:

	friend class YYNODESTATE;

	divide(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~divide();

};

class power : public binary
{
protected:

	friend class YYNODESTATE;

	power(YYNODESTATE *state__, expression * expr1, expression * expr2);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~power();

};

class negate : public unary
{
protected:

	friend class YYNODESTATE;

	negate(YYNODESTATE *state__, expression * expr);

public:

	virtual eval_value eval_expr();

	virtual int isA(int kind) const;
	virtual const char *getKindName() const;

protected:

	virtual ~negate();

};


void coerce(eval_value * value, type_code from, type_code to);
void infer_type(expression * e);



/*
 * Inherit YYNODESTATE to provide additional functionality.
 */
class NodeState : public YYNODESTATE
{
private:

	char *progname;
	char *filename;
	long  linenum;

public:

	NodeState(char *_progname, char *_filename)
			: YYNODESTATE()
		{
			filename = _filename;
			linenum = 1;
		}
	virtual ~NodeState();

public:

	virtual char *currFilename();
	virtual long currLinenum();
	virtual void failed();

	void incLine() { ++linenum; }

};

#endif
