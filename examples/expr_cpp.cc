/* expr_cpp.cc.  Generated automatically by treecc */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;
#include "expr_cpp.h"


#define YYNODESTATE_REENTRANT 1
#define YYNODESTATE_TRACK_LINES 1
#define YYNODESTATE_USE_ALLOCATOR 1
/*
 * treecc node allocation routines for C++.
 *
 * Copyright (C) 2001  Southern Storm Software, Pty Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * As a special exception, when this file is copied by treecc into
 * a treecc output file, you may use that output file without restriction.
 */

#ifndef YYNODESTATE_BLKSIZ
#define	YYNODESTATE_BLKSIZ	2048
#endif

/*
 * Types used by the allocation routines.
 */
struct YYNODESTATE_block
{
	char data__[YYNODESTATE_BLKSIZ];
	struct YYNODESTATE_block *next__;

};
struct YYNODESTATE_push
{
	struct YYNODESTATE_push *next__;
	struct YYNODESTATE_block *saved_block__;
	int saved_used__;
};

/*
 * Initialize the singleton instance.
 */
#ifndef YYNODESTATE_REENTRANT
YYNODESTATE *YYNODESTATE::state__ = 0;
#endif

/*
 * Some macro magic to determine the default alignment
 * on this machine.  This will compile down to a constant.
 */
#define	YYNODESTATE_ALIGN_CHECK_TYPE(type,name)	\
	struct _YYNODESTATE_align_##name { \
		char pad; \
		type field; \
	}
#define	YYNODESTATE_ALIGN_FOR_TYPE(type)	\
	((unsigned)(&(((struct _YYNODESTATE_align_##type *)0)->field)))
#define	YYNODESTATE_ALIGN_MAX(a,b)	\
	((a) > (b) ? (a) : (b))
#define	YYNODESTATE_ALIGN_MAX3(a,b,c) \
	(YYNODESTATE_ALIGN_MAX((a), YYNODESTATE_ALIGN_MAX((b), (c))))
YYNODESTATE_ALIGN_CHECK_TYPE(int, int);
YYNODESTATE_ALIGN_CHECK_TYPE(long, long);
#if defined(WIN32) && !defined(__CYGWIN__)
YYNODESTATE_ALIGN_CHECK_TYPE(__int64, long_long);
#else
YYNODESTATE_ALIGN_CHECK_TYPE(long long, long_long);
#endif
YYNODESTATE_ALIGN_CHECK_TYPE(void *, void_p);
YYNODESTATE_ALIGN_CHECK_TYPE(float, float);
YYNODESTATE_ALIGN_CHECK_TYPE(double, double);
#define	YYNODESTATE_ALIGNMENT	\
	YYNODESTATE_ALIGN_MAX( \
			YYNODESTATE_ALIGN_MAX3	\
			(YYNODESTATE_ALIGN_FOR_TYPE(int), \
		     YYNODESTATE_ALIGN_FOR_TYPE(long), \
			 YYNODESTATE_ALIGN_FOR_TYPE(long_long)), \
  	     YYNODESTATE_ALIGN_MAX3 \
		 	(YYNODESTATE_ALIGN_FOR_TYPE(void_p), \
			 YYNODESTATE_ALIGN_FOR_TYPE(float), \
			 YYNODESTATE_ALIGN_FOR_TYPE(double)))

/*
 * Constructor for YYNODESTATE.
 */
YYNODESTATE::YYNODESTATE()
{
	/* Initialize the allocation state */
	blocks__ = 0;
	push_stack__ = 0;
	used__ = 0;

#ifndef YYNODESTATE_REENTRANT
	/* Register this object as the singleton instance */
	if(!state__)
	{
		state__ = this;
	}
#endif
}

/*
 * Destructor for YYNODESTATE.
 */
YYNODESTATE::~YYNODESTATE()
{
	/* Free all node memory */
	clear();

#ifndef YYNODESTATE_REENTRANT
	/* We are no longer the singleton instance */
	if(state__ == this)
	{
		state__ = 0;
	}
#endif
}

#ifdef YYNODESTATE_USE_ALLOCATOR

/*
 * Allocate a block of memory.
 */
void *YYNODESTATE::alloc(size_t size__)
{
	struct YYNODESTATE_block *block__;
	void *result__;

	/* Round the size to the next alignment boundary */
	size__ = (size__ + YYNODESTATE_ALIGNMENT - 1) &
				~(YYNODESTATE_ALIGNMENT - 1);

	/* Do we need to allocate a new block? */
	block__ = blocks__;
	if(!block__ || (used__ + size__) > YYNODESTATE_BLKSIZ)
	{
		if(size__ > YYNODESTATE_BLKSIZ)
		{
			/* The allocation is too big for the node pool */
			return (void *)0;
		}
		block__ = new YYNODESTATE_block;
		if(!block__)
		{
			/* The system is out of memory.  The programmer can
			   inherit the "failed" method to report the
			   out of memory state and/or abort the program */
			failed();
			return (void *)0;
		}
		block__->next__ = blocks__;
		blocks__ = block__;
		used__ = 0;
	}

	/* Allocate the memory and return it */
	result__ = (void *)(block__->data__ + used__);
	used__ += size__;
	return result__;
}

/*
 * Deallocate a block of memory.
 */
void YYNODESTATE::dealloc(void *ptr__, size_t size__)
{
	/* Nothing to do for this type of node allocator */
}

/*
 * Push the node allocation state.
 */
int YYNODESTATE::push()
{
	struct YYNODESTATE_block *saved_block__;
	int saved_used__;
	struct YYNODESTATE_push *push_item__;

	/* Save the current state of the node allocation pool */
	saved_block__ = blocks__;
	saved_used__ = used__;

	/* Allocate space for a push item */
	push_item__ = (struct YYNODESTATE_push *)
			alloc(sizeof(struct YYNODESTATE_push));
	if(!push_item__)
	{
		return 0;
	}

	/* Copy the saved information to the push item */
	push_item__->saved_block__ = saved_block__;
	push_item__->saved_used__ = saved_used__;

	/* Add the push item to the push stack */
	push_item__->next__ = push_stack__;
	push_stack__ = push_item__;
	return 1;
}

/*
 * Pop the node allocation state.
 */
void YYNODESTATE::pop()
{
	struct YYNODESTATE_push *push_item__;
	struct YYNODESTATE_block *saved_block__;
	struct YYNODESTATE_block *temp_block__;

	/* Pop the top of the push stack */
	push_item__ = push_stack__;
	if(push_item__ == 0)
	{
		saved_block__ = 0;
		used__ = 0;
	}
	else
	{
		saved_block__ = push_item__->saved_block__;
		used__ = push_item__->saved_used__;
		push_stack__ = push_item__->next__;
	}

	/* Free unnecessary blocks */
	while(blocks__ != saved_block__)
	{
		temp_block__ = blocks__;
		blocks__ = temp_block__->next__;
		delete temp_block__;
	}
}

/*
 * Clear the node allocation pool completely.
 */
void YYNODESTATE::clear()
{
	struct YYNODESTATE_block *temp_block__;
	while(blocks__ != 0)
	{
		temp_block__ = blocks__;
		blocks__ = temp_block__->next__;
		delete temp_block__;
	}
	push_stack__ = 0;
	used__ = 0;
}

#endif /* YYNODESTATE_USE_ALLOCATOR */

/*
 * Default implementation of functions which may be overridden.
 */
void YYNODESTATE::failed()
{
}

#ifdef YYNODESTATE_TRACK_LINES

char *YYNODESTATE::currFilename()
{
	return (char *)0;
}

long YYNODESTATE::currLinenum()
{
	return 0;
}

#endif
intnum *YYNODESTATE::intnumCreate(int num)
{
	void *buf__ = this->alloc(sizeof(intnum));
	if(buf__ == 0) return 0;
	return new (buf__) intnum(this, num);
}

floatnum *YYNODESTATE::floatnumCreate(float num)
{
	void *buf__ = this->alloc(sizeof(floatnum));
	if(buf__ == 0) return 0;
	return new (buf__) floatnum(this, num);
}

cast *YYNODESTATE::castCreate(type_code new_type, expression * expr)
{
	void *buf__ = this->alloc(sizeof(cast));
	if(buf__ == 0) return 0;
	return new (buf__) cast(this, new_type, expr);
}

plus *YYNODESTATE::plusCreate(expression * expr1, expression * expr2)
{
	void *buf__ = this->alloc(sizeof(plus));
	if(buf__ == 0) return 0;
	return new (buf__) plus(this, expr1, expr2);
}

minus *YYNODESTATE::minusCreate(expression * expr1, expression * expr2)
{
	void *buf__ = this->alloc(sizeof(minus));
	if(buf__ == 0) return 0;
	return new (buf__) minus(this, expr1, expr2);
}

multiply *YYNODESTATE::multiplyCreate(expression * expr1, expression * expr2)
{
	void *buf__ = this->alloc(sizeof(multiply));
	if(buf__ == 0) return 0;
	return new (buf__) multiply(this, expr1, expr2);
}

divide *YYNODESTATE::divideCreate(expression * expr1, expression * expr2)
{
	void *buf__ = this->alloc(sizeof(divide));
	if(buf__ == 0) return 0;
	return new (buf__) divide(this, expr1, expr2);
}

power *YYNODESTATE::powerCreate(expression * expr1, expression * expr2)
{
	void *buf__ = this->alloc(sizeof(power));
	if(buf__ == 0) return 0;
	return new (buf__) power(this, expr1, expr2);
}

negate *YYNODESTATE::negateCreate(expression * expr)
{
	void *buf__ = this->alloc(sizeof(negate));
	if(buf__ == 0) return 0;
	return new (buf__) negate(this, expr);
}

expression::expression(YYNODESTATE *state__)
{
	this->kind__ = expression_kind;
	this->filename__ = state__->currFilename();
	this->linenum__ = state__->currLinenum();
	this->type = error_type;
}

expression::~expression()
{
	// not used
}

int expression::isA(int kind) const
{
	if(kind == expression_kind)
		return 1;
	else
		return 0;
}

const char *expression::getKindName() const
{
	return "expression";
}

binary::binary(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: expression(state__)
{
	this->kind__ = binary_kind;
	this->expr1 = expr1;
	this->expr2 = expr2;
}

binary::~binary()
{
	// not used
}

int binary::isA(int kind) const
{
	if(kind == binary_kind)
		return 1;
	else
		return expression::isA(kind);
}

const char *binary::getKindName() const
{
	return "binary";
}

unary::unary(YYNODESTATE *state__, expression * expr)
	: expression(state__)
{
	this->kind__ = unary_kind;
	this->expr = expr;
}

unary::~unary()
{
	// not used
}

int unary::isA(int kind) const
{
	if(kind == unary_kind)
		return 1;
	else
		return expression::isA(kind);
}

const char *unary::getKindName() const
{
	return "unary";
}

intnum::intnum(YYNODESTATE *state__, int num)
	: expression(state__)
{
	this->kind__ = intnum_kind;
	this->num = num;
}

intnum::~intnum()
{
	// not used
}

eval_value intnum::eval_expr()
{
	eval_value value;
	value.int_value = num;
	return value;
}

int intnum::isA(int kind) const
{
	if(kind == intnum_kind)
		return 1;
	else
		return expression::isA(kind);
}

const char *intnum::getKindName() const
{
	return "intnum";
}

floatnum::floatnum(YYNODESTATE *state__, float num)
	: expression(state__)
{
	this->kind__ = floatnum_kind;
	this->num = num;
}

floatnum::~floatnum()
{
	// not used
}

eval_value floatnum::eval_expr()
{
	eval_value value;
	value.float_value = num;
	return value;
}

int floatnum::isA(int kind) const
{
	if(kind == floatnum_kind)
		return 1;
	else
		return expression::isA(kind);
}

const char *floatnum::getKindName() const
{
	return "floatnum";
}

cast::cast(YYNODESTATE *state__, type_code new_type, expression * expr)
	: expression(state__)
{
	this->kind__ = cast_kind;
	this->new_type = new_type;
	this->expr = expr;
}

cast::~cast()
{
	// not used
}

eval_value cast::eval_expr()
{
	/* Evaluate the sub-expression */
	eval_value value = expr->eval_expr();

	/* Cast to the final type */
	coerce(&value, expr->type, type);

	/* Return the result to the caller */
	return value;
}

int cast::isA(int kind) const
{
	if(kind == cast_kind)
		return 1;
	else
		return expression::isA(kind);
}

const char *cast::getKindName() const
{
	return "cast";
}

plus::plus(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: binary(state__, expr1, expr2)
{
	this->kind__ = plus_kind;
}

plus::~plus()
{
	// not used
}

eval_value plus::eval_expr()
{
	/* Evaluate the sub-expressions */
	eval_value value1 = expr1->eval_expr();
	eval_value value2 = expr2->eval_expr();

	/* Coerce to the common type */
	coerce(&value1, expr1->type, type);
	coerce(&value2, expr2->type, type);

	/* Evaluate the operator */
	if(type == int_type)
	{
		value1.int_value += value2.int_value;
	}
	else
	{
		value1.float_value += value2.float_value;
	}

	/* Return the result to the caller */
	return value1;
}

int plus::isA(int kind) const
{
	if(kind == plus_kind)
		return 1;
	else
		return binary::isA(kind);
}

const char *plus::getKindName() const
{
	return "plus";
}

minus::minus(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: binary(state__, expr1, expr2)
{
	this->kind__ = minus_kind;
}

minus::~minus()
{
	// not used
}

eval_value minus::eval_expr()
{
	/* Evaluate the sub-expressions */
	eval_value value1 = expr1->eval_expr();
	eval_value value2 = expr2->eval_expr();

	/* Coerce to the common type */
	coerce(&value1, expr1->type, type);
	coerce(&value2, expr2->type, type);

	/* Evaluate the operator */
	if(type == int_type)
	{
		value1.int_value -= value2.int_value;
	}
	else
	{
		value1.float_value -= value2.float_value;
	}

	/* Return the result to the caller */
	return value1;
}

int minus::isA(int kind) const
{
	if(kind == minus_kind)
		return 1;
	else
		return binary::isA(kind);
}

const char *minus::getKindName() const
{
	return "minus";
}

multiply::multiply(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: binary(state__, expr1, expr2)
{
	this->kind__ = multiply_kind;
}

multiply::~multiply()
{
	// not used
}

eval_value multiply::eval_expr()
{
	/* Evaluate the sub-expressions */
	eval_value value1 = expr1->eval_expr();
	eval_value value2 = expr2->eval_expr();

	/* Coerce to the common type */
	coerce(&value1, expr1->type, type);
	coerce(&value2, expr2->type, type);

	/* Evaluate the operator */
	if(type == int_type)
	{
		value1.int_value *= value2.int_value;
	}
	else
	{
		value1.float_value *= value2.float_value;
	}

	/* Return the result to the caller */
	return value1;
}

int multiply::isA(int kind) const
{
	if(kind == multiply_kind)
		return 1;
	else
		return binary::isA(kind);
}

const char *multiply::getKindName() const
{
	return "multiply";
}

divide::divide(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: binary(state__, expr1, expr2)
{
	this->kind__ = divide_kind;
}

divide::~divide()
{
	// not used
}

eval_value divide::eval_expr()
{
	/* Evaluate the sub-expressions */
	eval_value value1 = expr1->eval_expr();
	eval_value value2 = expr2->eval_expr();

	/* Coerce to the common type */
	coerce(&value1, expr1->type, type);
	coerce(&value2, expr2->type, type);

	/* Evaluate the operator */
	if(type == int_type)
	{
		if(value2.int_value != 0)
		{
			value1.int_value /= value2.int_value;
		}
		else
		{
			cerr << getFilename() << ":" << getLinenum() <<
					": division by zero" << endl;
			value1.int_value = 0;
		}
	}
	else
	{
		value1.float_value /= value2.float_value;
	}

	/* Return the result to the caller */
	return value1;
}

int divide::isA(int kind) const
{
	if(kind == divide_kind)
		return 1;
	else
		return binary::isA(kind);
}

const char *divide::getKindName() const
{
	return "divide";
}

power::power(YYNODESTATE *state__, expression * expr1, expression * expr2)
	: binary(state__, expr1, expr2)
{
	this->kind__ = power_kind;
}

power::~power()
{
	// not used
}

eval_value power::eval_expr()
{
	/* Evaluate the sub-expressions */
	eval_value value1 = expr1->eval_expr();
	eval_value value2 = expr2->eval_expr();

	/* Evaluate the operator */
	if(type == int_type)
	{
		value1.int_value = (int)(pow((double)(value1.int_value),
		                             (double)(value2.int_value)));
	}
	else
	{
		value1.float_value = (float)(pow((double)(value1.float_value),
		                                 (double)(value2.int_value)));
	}

	/* Return the result to the caller */
	return value1;
}

int power::isA(int kind) const
{
	if(kind == power_kind)
		return 1;
	else
		return binary::isA(kind);
}

const char *power::getKindName() const
{
	return "power";
}

negate::negate(YYNODESTATE *state__, expression * expr)
	: unary(state__, expr)
{
	this->kind__ = negate_kind;
}

negate::~negate()
{
	// not used
}

eval_value negate::eval_expr()
{
	/* Evaluate the sub-expression */
	eval_value value = expr->eval_expr();

	/* Evaluate the operator */
	if(type == int_type)
	{
		value.int_value = -(value.int_value);
	}
	else
	{
		value.float_value = -(value.float_value);
	}

	/* Return the result to the caller */
	return value;
}

int negate::isA(int kind) const
{
	if(kind == negate_kind)
		return 1;
	else
		return unary::isA(kind);
}

const char *negate::getKindName() const
{
	return "negate";
}

void coerce(eval_value * value, type_code from, type_code to)
{
	switch(from)
	{
		case error_type:
		{
			switch(to)
			{
				case error_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				case int_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				case float_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				default: break;
			}
		}
		break;

		case int_type:
		{
			switch(to)
			{
				case error_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				case int_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				case float_type:
				{
					{
						value->float_value = (float)(value->int_value);
					}
				}
				break;

				default: break;
			}
		}
		break;

		case float_type:
		{
			switch(to)
			{
				case error_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				case int_type:
				{
					{
						value->int_value = (int)(value->float_value);
					}
				}
				break;

				case float_type:
				{
					{
						/* Nothing to do here */
					}
				}
				break;

				default: break;
			}
		}
		break;

		default: break;
	}
}

static void infer_type_1__(power *e)
{
    infer_type(e->expr1);
    infer_type(e->expr2);

	if(e->expr1->type == error_type || e->expr2->type == error_type)
	{
		e->type = error_type;
	}
    else if(e->expr2->type != int_type)
    {
		cerr << e->getFilename() << ":" << e->getLinenum() <<
        		": second argument to `^' is not an integer" << endl;
		e->type = error_type;
    }
	else
	{
    	e->type = e->expr1->type;
	}
}

static void infer_type_2__(binary *e)
{
    infer_type(e->expr1);
    infer_type(e->expr2);

    if(e->expr1->type == error_type || e->expr2->type == error_type)
	{
        e->type = error_type;
	}
    else if(e->expr1->type == float_type || e->expr2->type == float_type)
    {
        e->type = float_type;
    }
    else
    {
        e->type = int_type;
    }
}

static void infer_type_3__(unary *e)
{
    infer_type(e->expr);
    e->type = e->expr->type;
}

static void infer_type_4__(intnum *e)
{
    e->type = int_type;
}

static void infer_type_5__(floatnum *e)
{
    e->type = float_type;
}

static void infer_type_6__(cast *e)
{
	infer_type(e->expr);

	if(e->expr->type != error_type)
	{
		e->type = e->new_type;
	}
	else
	{
		e->type = error_type;
	}
}

void infer_type(expression * e__)
{
	switch(e__->getKind())
	{
		case power_kind:
		{
			infer_type_1__((power *)e__);
		}
		break;

		case binary_kind:
		case plus_kind:
		case minus_kind:
		case multiply_kind:
		case divide_kind:
		{
			infer_type_2__((binary *)e__);
		}
		break;

		case unary_kind:
		case negate_kind:
		{
			infer_type_3__((unary *)e__);
		}
		break;

		case intnum_kind:
		{
			infer_type_4__((intnum *)e__);
		}
		break;

		case floatnum_kind:
		{
			infer_type_5__((floatnum *)e__);
		}
		break;

		case cast_kind:
		{
			infer_type_6__((cast *)e__);
		}
		break;

		default: break;
	}
}



/*
 * Entry points that are imported from the yacc parser.
 */
extern void yyrestart(FILE *file);
extern int yyparse(void *);

/*
 * Main entry point for the expression parser and evaluator.
 */
int main(int argc, char *argv[])
{
	FILE *file;
	char *filename;
	int retval;

	/* Parse the command-line arguments and open the input file */
	if(argc < 2)
	{
		filename = "stdin";
		file = stdin;
	}
	else if((file = fopen(argv[1], "r")) == NULL)
	{
		perror(argv[1]);
		return 1;
	}
	else
	{
		filename = argv[1];
	}

	/* Create the node factory and state object */
	NodeState *state = new NodeState(argv[0], filename);

	/* Parse and evaluate the expressions in the input */
	retval = yyparse(state);

	/* Clean up and exit */
	delete state;
	return retval;
}

/*
 * Destructor for NodeState.
 */
NodeState::~NodeState()
{
	/* Nothing needs to be done here */
}

/*
 * Get the name of the current input file in use by the parser.
 */
char *NodeState::currFilename()
{
	return filename;
}

/*
 * Get the line number for the current input line in use by the parser.
 */
long NodeState::currLinenum()
{
	return linenum;
}

/*
 * Report memory failure and exit.
 */
void NodeState::failed()
{
	cerr << progname << ": virtual memory exhausted" << endl;
	exit(1);
}

