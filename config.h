#pragma once

#define HAVE_STRING_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STDARG_H 1
//#define HAVE_VARARGS_H


#define HAVE_VFPRINTF  1
#define HAVE_STRCHR    1
#define HAVE_QSORT     1
#define HAVE_VSNPRINTF 1
#define HAVE_VSPRINTF 1
#define HAVE_MEMCMP 1
#define HAVE_MEMCPY 1


#define VERSION "3.11"
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

